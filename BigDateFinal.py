 #!/usr/bin/env python3
 import tweepy
 import sys
 from tweepy import OAuthHandler
 from tweepy import API
 from tweepy import Stream
 from tweepy.streaming import StreamListener
 
 
 #I removed my keys & Tokens from here
 ACCESS_TOKEN = ""
 ACCESS_TOKEN_SECRET = ""
 CONSUMER_KEY = ""
 CONSUMER_SECRET = ""
 
 auth = OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
 auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
 api = API(auth, wait_on_rate_limit=True,
           wait_on_rate_limit_notify=True)
 
+#This creates the listener which will basically change all the large outputs into something much more easier to read
 class Listener(StreamListener):
     def __init__(self, output_file=sys.stdout):
         super(Listener,self).__init__()
         self.output_file = output_file
     def on_status(self, status):
         print(status.text, file=self.output_file)
     def on_error(self, status_code):
         print(status_code)
         return False
 
+#This is where the information is stored
 output = open('Streamed_Content.txt', 'w')
 listener = Listener(output_file=output)
 stream = Stream(auth=api.auth, listener=listener)
 
 try:
+    print('Beginning the stream:')
     stream.filter(track=["NBA"])
+    #This filters all the results to include tweets that contain NBA in them.
 except KeyboardInterrupt:
+    print("Ended.")
 finally:
     print('Done.')
     stream.disconnect()
     output.close()